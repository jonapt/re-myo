<p align="center">
![Logo](https://gitlab.com/jonapt/re-myo/-/raw/main/img/Setup_ico.png?ref_type=heads){ width=25% height=25% }
</p>


# EN CONSTRUCCIÓN
# Re-Myo
## tareas pendientes 

- [ ] Implementación en linux
- [ ] Revisión de repositorios
- [ ] Crear grabador de señales

## Recusos tomados de:

* [PyoMyo](https://github.com/PerlinWarp/pyomyo)
* [Myo-python](https://github.com/PerlinWarp/pyomyo)


ejecutar este comando en el terminal de Windows para permitir que se pueda ejecutar el Script 
```
Set-ExecutionPolicy Unrestricted
```
# Descripción del Producto: Myo Armband
El Myo Armband es un dispositivo portátil diseñado para captar los gestos y movimientos del brazo del usuario, permitiendo el control de dispositivos electrónicos a través de señales musculares y gestos. Desarrollado originalmente por Thalmic Labs, el Myo Armband se lanzó con el objetivo de ofrecer una nueva forma de interacción con la tecnología, especialmente en áreas como la realidad aumentada, la realidad virtual, la domótica, y aplicaciones industriales.

## Componentes de Hardware
### Diseño y Estructura
El Myo Armband es una banda elástica que se ajusta al antebrazo del usuario. Está compuesto por ocho módulos que contienen electrodos capaces de detectar las señales eléctricas generadas por los músculos durante el movimiento. Estos módulos están conectados a una unidad central que procesa los datos en tiempo real.
### Especificaciones Técnicas
    1. Sensores EMG (Electromiografía): Los ocho módulos del Myo Armband incluyen sensores 
    de EMG que detectan la actividad eléctrica generada por los músculos.

    2. Acelerómetro, giroscopio y magnetómetro: Estos sensores permiten rastrear la 
    orientación, aceleración y posición del brazo en el espacio.

    3. Conectividad Bluetooth 4.0: El Myo Armband se conecta de forma inalámbrica a dispositivos
    compatibles mediante Bluetooth, lo que permite la transmisión de datos en tiempo real.

    4. Batería Recargable: Incluye una batería de polímero de litio que ofrece hasta 8 horas
    de uso continuo.

    5. Peso: El dispositivo es ligero, pesando aproximadamente 93 gramos.




# Definiciones EMG
## Origen fisiológico de los bioseñales:
Las células nerviosas y musculares generan señales bioeléctricas que son el resultado de 
cambios electroquímicos entre las células. Si la célula nerviosa o muscular son lo 
suficientemente estimulados como para alcanzar cierto umbral, la célula generará un 
potencial de acción. Dicho estímulo tiene su origen debido a un cambio en la 
permeabilidad de la membrana neuronal a ciertos iones, principalmente el sodio (Na+) y 
el potasio (K+)

• Estado de reposo: La membrana de la neurona tiene una carga negativa en su 
interior y positiva en el exterior, manteniendo un potencial de reposo de 
aproximadamente -70 mV.

• Despolarización: Cuando un estímulo alcanza el umbral (alrededor de -55 mV), 
se abren los canales de sodio (Na+), permitiendo que los iones de sodio entren 
en la célula. Esto hace que el interior de l

• Repolarización: Los canales de sodio se cierran y se abren los canales de potasio 
(K+), permitiendo que los iones de potasio salgan de la célula. Esto devuelve la 
carga negativa al interior de la célula.

## Utilidades del EMG
    1. Diagnóstico de Trastornos Neuromusculares: La EMG ayuda a identificar enfermedades 
    que afectan los músculos y los nervios, como la esclerosis lateral amiotrófica (ELA), la 
    miastenia gravis y las neuropatías periféricas.
    
    2. Evaluación de Lesiones Nerviosas: Permite localizar y evaluar el grado de daño en los 
    nervios, diferenciando entre neuropatías axonales y desmielinizantes.
    
    3. Detección de Debilidad Muscular: Ayuda a distinguir si la debilidad muscular es de origen 
    central (cerebro o médula espinal) o periférico (nervios o músculos).
    
    4. Monitoreo de la Recuperación: Se utiliza para seguir la evolución de la recuperación de 
    lesiones nerviosas y musculares, proporcionando información sobre el pronóstico.

## Aplicaciones Clínicas.
    1. Neurología: Diagnóstico y seguimiento de enfermedades neuromusculares.
    2. Ortopedia: Evaluación de lesiones nerviosas asociadas a fracturas o cirugías.
    3. Rehabilitación: Planificación de terapias y seguimiento de la recuperación funcional.
    4. Medicina Deportiva: Diagnóstico de lesiones deportivas y planificación de la rehabilitación.

[Seguir leyendo](https://gitlab.com/jonapt/re-myo/-/blob/main/Fundamentos%20teoricos%20EMG.pdf?ref_type=heads)

# Repositorios pendientes a revisar
- [MyoLinux](https://github.com/brokenpylons/MyoLinux)
- [myo-bluetooth](https://github.com/thalmiclabs/myo-bluetooth)
- [Myo4Linux](https://github.com/Ramir0/Myo4Linux)



