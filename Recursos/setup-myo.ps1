#Instalar Myo Connect
Write-Host "Instalando Myo-Connet"
."SDK INSTAL\Myo+Connect+Installer.exe"

Write-Host "Permisos de ejecución"
Set-ExecutionPolicy Unrestricted
# Instalar python 3.11
Write-Host "Instalando Python 3.11"
winget install 9NCVDN91XZQP
#Instalar librearias necesarias
Write-Host "Instalando librerias necesarias"
$directorioActual = Get-Location
python -m pip install -r "$($directorioActual.Path)\requirements.txt"



# Añadir Sdk de Myo al Path
Write-Host "Añadiendo Myo-SDK"
$sdk = "$($directorioActual.Path)SDK-myo\myo-sdk-win-0.9.0\bin"
$Path = [Environment]::GetEnvironmentVariable("Path", "Machine")
$Path += $sdk
[Environment]::SetEnvironmentVariable("Path", $Path, "Machine")
Write-Host $Path


