from collections import deque
from threading import Lock, Thread
import time
import json
import paho.mqtt.client as mqtt

import myo
import numpy as np




class EmgCollector(myo.DeviceListener):
    """
    Collects EMG data in a queue with *n* maximum number of elements.
    """

    def __init__(self, n):
        self.n = n
        self.lock = Lock()
        self.emg_data_queue = deque(maxlen=n)

    def get_emg_data(self):
        with self.lock:
            return list(self.emg_data_queue)

    # myo.DeviceListener

    def on_connected(self, event):
        event.device.stream_emg(True)

    def on_emg(self, event):
        with self.lock:
            self.emg_data_queue.append((event.emg))


class PrintEMG(object):
    
    def __init__(self, listener):
        self.listener = listener

    def print_emg_data(self):
        # Configura el cliente MQTT
        client = mqtt.Client()
        # Conecta al broker MQTT
        client.connect("54.160.245.112", 1883, 60)

        c=0

        while True:
            emg_data = self.listener.get_emg_data()
            # emg_data = np.array([x[1] for x in emg_data])
            
            for i in emg_data:
                json_data={
                    f"emg_data_1": i[0],
                    f"emg_data_2": i[1],
                    f"emg_data_3": i[2],
                    f"emg_data_4": i[3],
                    f"emg_data_5": i[4],
                    f"emg_data_6": i[5],
                    f"emg_data_7": i[6],
                    f"emg_data_8": i[7],
                }
                emg_json = json.dumps(json_data)
                client.publish("hola1", emg_json)
                print(emg_json)

            print("\n")
            # Adjust the sleep time to control the rate of printing
            # For example, 1.0 / 30 will print roughly 30 times per second
            # You can adjust this value according to your preference
            c=c+1
            time.sleep(0.1)
        client.disconnect()




def main():
    myo.init()
    hub = myo.Hub()
    listener = EmgCollector(512)
    with hub.run_in_background(listener.on_event):
        PrintEMG(listener).print_emg_data()


if __name__ == '__main__':
    main()
