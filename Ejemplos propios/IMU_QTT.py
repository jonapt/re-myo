from __future__ import print_function
from myo.utils import TimeInterval
import myo
import sys
import json
import paho.mqtt.client as mqtt

class Listener(myo.DeviceListener):
  

  def __init__(self):
    self.interval = TimeInterval(None, 0.05)
    self.orientation = None

  def output_orientation(self):
    client = mqtt.Client()
    # Conecta al broker MQTT
    client.connect("192.168.129.209", 1883, 60)
    if not self.interval.check_and_reset():
      return

    parts = []
    if self.orientation:
      for comp in self.orientation:
        parts.append('{}{:.4f}'.format(' ' if comp >= 0 else '', comp))
    #print('Orientation: ' + ' '.join(parts))
    orient_= parts
    json_data={
      'imu_1':orient_[0],
      'imu_2':orient_[1],
      'imu_3':orient_[2],
      'imu_4':orient_[3],

    }
    emg_json = json.dumps(json_data)
    client.publish("hola1", emg_json)
    print(json_data)
    sys.stdout.flush()

  def on_orientation(self, event):
    self.orientation = event.orientation
    self.output_orientation()

if __name__ == '__main__':
  myo.init()
  hub = myo.Hub()
  listener = Listener()
  while hub.run(listener.on_event, 500):
    pass